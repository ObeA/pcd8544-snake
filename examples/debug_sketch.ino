#include <Snake.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

#define SNAKE_DIRECTION_NORTH_PIN 8
#define SNAKE_DIRECTION_SOUTH_PIN 9
#define SNAKE_DIRECTION_WEST_PIN 10
#define SNAKE_DIRECTION_EAST_PIN 11

// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out (SCLK)
// pin 6 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
Snake snake = Snake();

void setup()   {
  Serial.begin(9600);
  
  pinMode(8, INPUT);
  pinMode(9, INPUT);
  pinMode(10, INPUT);
  pinMode(11, INPUT);
  
  snake.display.begin();
  snake.display.setContrast(50);
  snake.display.clearDisplay();
  snake.initialiseGame();
  snake.display.display();
}

int lastDirectionChange;
void loop() {
  if (millis() - lastDirectionChange > 200)
  {
    Direction currDirection = snake.getDirection();
    switch (snake.getDirection())
    {
      case NORTH:
      case SOUTH:
        if (digitalRead(8))
          snake.setDirection(WEST);
        else if (digitalRead(9))
          snake.setDirection(EAST);
      break;
      case WEST:
      case EAST:
        if (digitalRead(8))
          snake.setDirection(NORTH);
        else if (digitalRead(9))
          snake.setDirection(SOUTH);
      break; 
    }
    if (currDirection != snake.getDirection()) {
      lastDirectionChange = millis();
      Serial.println(directionToString(currDirection) + " -> " + directionToString(snake.getDirection()));
    }
  }
  
  snake.updateSnakePosition();
  snake.drawSnake();
  snake.display.display();
}

String directionToString(Direction dir) {
 switch (dir) {
  case NORTH:
    return "NORTH";
  case SOUTH:
    return "SOUTH";
  case EAST:
    return "EAST";
  case WEST:
    return "WEST";
  default:
    return "UNKNOWN";
 } 
}
