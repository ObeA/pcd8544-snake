#include "Snake.h"
#include "Adafruit_GFX.h"
#include "Adafruit_PCD8544.h"

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

// GAME SPECIFIC SETTINGS


// END GAME SPECIFIC SETTINGS

// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out (SCLK)
// pin 6 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)

void Snake::initialiseGame() {
  drawBorder();

  for (int i = 0; i < snakeLength; i++)
  {
    segmentPosition[i].x = LCDWIDTH / 2 + ((i + 1) * SNAKE_SEGMENT_WIDTH);
    segmentPosition[i].y = LCDHEIGHT / 2;
  }

  drawSnake();
  spawnFood();
  drawFood();
}

void Snake::drawSegment(Point p) {
  //if (p.x = segmentPosition[0].x && p.y == segmentPosition[0].y)
  //  display.fillRect(p.x, p.y, SNAKE_SEGMENT_WIDTH, SNAKE_SEGMENT_HEIGHT, COLOUR);
  //else
    display.drawRect(p.x, p.y, SNAKE_SEGMENT_WIDTH, SNAKE_SEGMENT_HEIGHT, COLOUR);
}
void Snake::clearSegment(Point p) {
  display.fillRect(p.x, p.y, SNAKE_SEGMENT_WIDTH, SNAKE_SEGMENT_HEIGHT, 0);
}

void Snake::setDirection(Direction newDirection) {
    switch (newDirection) {
    case NORTH:
        if (direction != SOUTH)
            direction = newDirection;
        break;
    case SOUTH:
        if (direction != NORTH)
            direction = newDirection;
        break;
    case WEST:
        if (direction != EAST)
            direction = newDirection;
        break;
    case EAST:
        if (direction != WEST)
            direction = newDirection;
        break;
    }
}
Direction Snake::getDirection() {
    return direction;
}
void Snake::updateSnakePosition() {
  if (millis() - lastUpdate < speed)
    return;

  clearSegment(segmentPosition[snakeLength - 1]);
  for (int i = snakeLength - 1; i > 0; i--)
  {
    segmentPosition[i].x = segmentPosition[i - 1].x;
    segmentPosition[i].y = segmentPosition[i - 1].y;
  }

  switch(direction)
  {
    NORTH:
      // Increase Y;
      segmentPosition[0].y += SNAKE_SEGMENT_HEIGHT;
      break;
    SOUTH:
      // Decrease Y;
      segmentPosition[0].y -= SNAKE_SEGMENT_HEIGHT;
      break;
    EAST:
      // Increase X;
      segmentPosition[0].x += SNAKE_SEGMENT_WIDTH;
      break;
    default:
    WEST:
      // Decrease X;
      segmentPosition[0].x -= SNAKE_SEGMENT_WIDTH;
      break;
  }

  lastUpdate = millis();
}

void Snake::drawBorder() {
  display.drawRect(0, 0, LCDWIDTH, LCDHEIGHT, COLOUR);
}

void Snake::drawSnake() {
  for (int i = 0; i < snakeLength; i++)
  {
    drawSegment(segmentPosition[i]);
  }
}

void Snake::spawnFood() {
    randomSeed(analogRead(0));
    do {
        food.x = (uint8_t)random(1, LCDWIDTH - SNAKE_SEGMENT_WIDTH);
        food.y = (uint8_t)random(1, LCDHEIGHT - SNAKE_SEGMENT_HEIGHT);
    } while (collidesWithObject(food));
}

bool Snake::collidesWithObject(Point p) {
    return collidesWithBorder(p) || collidesWithSnake(p);
}

bool Snake::collidesWithBorder(Point p) {
  return !((p.x > 1 && p.y > 1)
        && (p.x < LCDWIDTH - 1 && p.y < LCDHEIGHT - 1));
}

bool Snake::collidesWithSnake(Point p) {
    for (int i = 0; i < snakeLength; i++)
    {
        if ((p.x >= segmentPosition[i].x && p.y >= segmentPosition[i].y) &&
            (p.x <= segmentPosition[i].x && p.y <= segmentPosition[i].y))
            return true;
    }

    return false;
}

void Snake::drawFood() {
    drawSegment(food);
}
