#include "Adafruit_GFX.h"
#include "Adafruit_PCD8544.h"

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#define SNAKE_MAX_LENGTH 50
#define SNAKE_START_LENGTH 10
#define SNAKE_SEGMENT_WIDTH 3
#define SNAKE_SEGMENT_HEIGHT 3
#define SNAKE_START_SPEED 180;
#define COLOUR 255

typedef struct Point {
  uint8_t x;
  uint8_t y;
} Point;

typedef enum {
  NORTH,
  SOUTH,
  EAST,
  WEST,
} Direction;

class Snake {
    public:
        Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 5, 4, 3); // FIX ME PLS

        uint16_t speed = SNAKE_START_SPEED;
        uint16_t score;

        void drawSegment(Point);
        void clearSegment(Point);
        void updateSnakePosition();
        bool collidesWithObject(Point);
        bool collidesWithBorder(Point);
        bool collidesWithSnake(Point);
        void spawnFood();
        void drawBorder();
        void drawSnake();
        void drawFood();
        void initialiseGame();
        void setDirection(Direction);
        Direction getDirection();
    private:
        Direction direction = WEST;
        Point food;
        uint16_t lastUpdate;
        uint8_t snakeLength = SNAKE_START_LENGTH;
        Point segmentPosition[SNAKE_MAX_LENGTH];
};
